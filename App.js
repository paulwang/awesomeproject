import React from 'react';
import { StyleSheet, Text, View,Image,TextInput } from 'react-native';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state= {name:'Paul Wang'}
    // setInterval(() => {
    //   this.setState({name:'王治'})
    // }, 1000);

  }
  render() {
    let pic = {
      uri: 'http://att2.citysbs.com/hangzhou/2017/06/21/10/199x276-104454_v2_14811498013094399_6af0c923564356d44b52e4eafebe9d5c.jpg'
    };
    let name = this.state.name
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <TextInput
          style={{height: 40}}
          placeholder="Type here to translate!"
          onChangeText={(text) => this.setState({name:text})}>
        </TextInput>
        <Text>Changes you make will automatically reload!</Text>
        <Text>Shake your phone to open the developer menu.</Text>
	       <Text> {this.state.name} &lt;wagzhi@gmail.com&gt; </Text>
        <Image source={pic} style={{width: 193, height: 110}} />
      </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


